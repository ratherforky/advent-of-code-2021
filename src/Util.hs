{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE TypeOperators #-}
{-# language NoImplicitPrelude, ScopedTypeVariables, TypeApplications, Strict #-}
module Util where

import MyStreamingPrelude
-- import Parsing

import qualified Streamly.Internal.Unicode.Stream as UnicodeS
import qualified Streamly.Data.Fold as Fold
import qualified Streamly.Internal.Data.Fold as FoldI
import qualified Streamly.Internal.FileSystem.File as File

import Control.Exception.Extra ( errorIO )

import Control.Monad.Except (ExceptT, runExceptT)
import Control.Monad.State.Strict (StateT, evalStateT)
-- import Streamly.Internal.Data.Parser as Parser
-- import Streamly.Internal.Unicode.Char.Parser

import qualified Debug.Trace as Debug
import Control.Applicative (Alternative(empty))


foldLines :: Fold IO Char b -> FilePath -> Serial b
foldLines alg path
  = unfold File.read path
 |> UnicodeS.decodeLatin1
 |> UnicodeS.lines alg

readFold :: (Read a, Monad m) => Fold m Char (Maybe a)
readFold = readMaybe <$> Fold.toList 

readInputLines :: Read a => FilePath -> Serial (Maybe a)
readInputLines = foldLines readFold

unsafeReadInputLines :: forall a . Read a => FilePath -> Serial a
unsafeReadInputLines
  = foldLines (Fold.rmapM justOrErr (readFold @a @IO))
  where
    justOrErr :: Maybe b -> IO b
    justOrErr (Just x) = pure x
    justOrErr Nothing  = errorIO "An input line failed to parse"

-- foo = takeWhile 

printDebug :: Show a => String -> a -> a
printDebug label x = Debug.trace (label ++ ": " ++ show x) x

readChars :: FilePath -> Serial Char
readChars path
  = unfold File.read path
 |> UnicodeS.decodeLatin1

-- From Relude (not that it's very complicated)
toSnd :: (a -> b) -> a -> (a, b)
toSnd f a = (a, f a)
{-# INLINE toSnd #-}

-- Folds

maximumOn :: (Monad m, Ord b) => (a -> b) -> Fold m a (Maybe a)
-- maximumOn f = Fold.maximumBy (\x y -> compare (f x) (f y))
maximumOn f = Fold.maximumBy (lcomp2 f compare)

-- Applies a function to both *inputs* of a binary function
lcomp2 :: (a -> b) -> (b -> b -> c) -> (a -> a -> c)
lcomp2 f bi = \x y -> bi (f x) (f y)

constFullFold :: Monad m => b -> Fold m a b
constFullFold x = FoldI.mkFold_ step (FoldI.Partial x)
  where
    -- step :: b -> a -> FoldI.Step b b
    step _ _ = FoldI.Partial x

-- untilThrow :: (Monad m) => s -> StateT s (ExceptT a m) a -> m a
-- untilThrow startState action = (runExceptT (evalStateT action startState))
--     >>= (\case
--   Left  a -> pure a
--   Right a ->  )

-- Data types

data a :+: b = a :+: b -- Strict tuple
  deriving stock (Eq, Ord, Show)
infixr 4 :+:

fst' :: (a :+: b) -> a
fst' (x :+: _) = x

snd' :: (a :+: b) -> b
snd' (_ :+: y) = y

-- Misc

guarded :: Alternative f => (a -> Bool) -> a -> f a
guarded p x
  | p x = pure x
  | otherwise = empty

toRatio :: Integral a => a -> Ratio a
toRatio = (% 1)

safeDivide :: (Eq a, Fractional a) => a -> a -> Maybe a
safeDivide x 0 = Nothing
safeDivide x y = Just (x / y)
