{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies, FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DerivingVia, StandaloneDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# language NoImplicitPrelude, LambdaCase, TypeApplications, ScopedTypeVariables #-}
{-# LANGUAGE PatternSynonyms, MultiWayIf, ApplicativeDo #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Day3 where

import MyStreamingPrelude -- Based on Streamly rather than lists

import Util
import Parsing

import qualified Streamly.Data.Fold as Fold


-- Typelevel bs, more for fun than anything
-- import Data.Vector.Fixed as Vec
-- import Data.Vector.Fixed.Storable
-- import Data.Vector.Fixed.Cont (Add, Peano, PeanoNum (..))
import GHC.TypeNats
import Data.Type.Natural
import Data.Vector.Unboxed.Sized (Vector) -- boxed vs. unboxed seems to make little difference
-- import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Sized as VS
-- import qualified Data.Vector.Generic.Sized as VGS
-- import qualified Data.Vector.Generic as VG
-- import qualified Data.Vector.Generic.Mutable.Base as VGMB
import Control.Exception (AssertionFailed(AssertionFailed))
import Control.Monad.Catch (MonadThrow(throwM))
import Streamly.Internal.Unicode.Char.Parser (char)
import Streamly.Internal.Data.Parser ( some )
import Data.Bits (Bits (complement))
import Data.Binary
import Data.Finite.Internal
import Control.Monad.Extra (loopM)
import Text.Printf (printf)

--------------------------------------------------
-- Get Input
--------------------------------------------------

type Bit = Bool
  -- deriving (Eq, Bits, Binary) via Bool

-- deriving via Bool instance VGMB.MVector V.MVector Bit
-- deriving via Bool instance VGS.Vector VS.Vector Bit
-- deriving via Bool instance Unbox Bit

-- instance Show Bit where
--   show Zer = "0"
--   show One = "1"

pattern Zer = False
pattern One = True

{-# COMPLETE Zer, One #-}

bitToInt :: Bit -> Int
bitToInt One = 1
bitToInt Zer = 0

exampleBits5 :: Maybe (Vector 5 Bit)
exampleBits5 = VS.fromListN [One, Zer, One, One, Zer]

exampleBits5' :: Vector 5 Bit
exampleBits5' = fromJust exampleBits5

bit :: MonadCatch m => Parser m Char Bit
bit = One <$ char '1'
  <|> Zer <$ char '0'

bitsN :: forall n m . (KnownNat n, MonadCatch m) => Parser m Char (Vector n Bit)
bitsN = some bit alg
  where
    alg :: Fold m Bit (Vector n Bit)
    alg = Fold.rmapM f Fold.toList

    f :: [Bit] -> m (Vector n Bit)
    f xs = case VS.fromListN @n xs of
      Nothing  -> throwM (AssertionFailed 
                           $ mconcat ["Must be exactly length ", (show (sNat @n))])
      Just vec -> pure vec


input :: Serial (Vector 12 Bit)
input = parseInputLines "src/inputs/day3.txt" (entirely bitsN)

exampleInput :: Serial (Vector 5 Bit)
exampleInput = parseInputLines "src/inputs/day3-example.txt" (entirely bitsN)

--------------------------------------------------
-- Task 1
--------------------------------------------------

runTask1 :: IO Int
runTask1 = input |> fold task1Fold

task1Fold :: Monad m => Fold m (Vector 12 Bit) Int
task1Fold
  = mostFreq
 |> fmap (\b -> toDec b * toDec (complementVec b))

complementVec :: Vector n Bit -> Vector n Bit
complementVec = VS.map complement

mostFreq :: (KnownNat n, Monad m) 
         => Fold m (Vector n Bit) (Vector n Bit)
mostFreq
  = Fold.lmap (VS.map bitToInt)
  $ Fold.teeWith (\len sumTotal -> VS.map (calcMostFreq len) sumTotal) 
        Fold.length
        zipSum 

zipSum :: (KnownNat n, Monad m) 
       => Fold m (Vector n Int) (Vector n Int)
zipSum = Fold.foldl' (VS.zipWith (+)) (VS.replicate 0)

-- This one simple function caused a lot of bugs
calcMostFreq :: Int -> Int -> Bit
calcMostFreq len freq
  | odd len  && freq <= (len `div` 2) = Zer
  | even len && freq <  (len `div` 2) = Zer
  | otherwise = One

toDec :: Vector n Bit -> Int
toDec xs = VS.foldl' (\acc b -> acc * 2 + bitToInt b) 0 xs

--------------------------------------------------
-- Task 2
--------------------------------------------------

runTask2 :: IO Int
runTask2
  = input
 |> task2

task2 :: (KnownNat n, n >= 1) => Serial (Vector n Bit) -> IO Int
task2 xs = do
  oxy <- loopM (go id) (0, xs)
  co2 <- loopM (go complement) (0, xs)
  pure $ toDec oxy * toDec co2

go :: (KnownNat n, n >= 1)
   => (Bit -> Bit)
   -> (Finite n, Serial (Vector n Bit))
   -> IO (Either (Finite n, Serial (Vector n Bit)) (Vector n Bit))
go f (i, xs) = do
  mostFreqVec <- fold mostFreq xs

  let target = f (VS.index mostFreqVec i)
      p ys   = VS.index ys i == target
      xs'    = filter p xs
  
  len <- length xs'

  if | len == 1         -> (fromJust .> Right) <$> (head xs')
     | i < maxFinite &&
       1 < len          -> pure $ Left (i + 1, xs')
     | otherwise -> do
        printf "i: %d, target: %d\n" (getFinite i) (bitToInt target)
        xs' |> mapM_ print
        putStrLn ""
        throwM (AssertionFailed "There must be a single element before end reached") 


-- Thought this was a clever hack for part 2, but actually it's just wrong lmao
notTask2 :: Serial (Vector 12 Bit) -> IO Int
notTask2 xs = do
  mostFreqVec <- fold mostFreq xs
  fold (notTask2Fold mostFreqVec) xs

notTask2Fold :: MonadCatch m => Vector 12 Bit -> Fold m (Vector 12 Bit) Int
notTask2Fold mostFreqVec
  = Fold.teeWith (\x y -> toDec x * toDec y)
      (highlanderMatch mostFreqVec)
      (highlanderMatch $ complementVec mostFreqVec)

-- There can be only one
highlanderMatch :: (KnownNat n, n >= 1, MonadCatch m) => Vector n Bit -> Fold m (Vector n Bit) (Vector n Bit)
highlanderMatch target
  = Fold.lmap (toSnd (countSeqMatches target)) (maximumOn snd)
 |> Fold.rmapM (\case
      Nothing -> throwM (AssertionFailed "There must be some max element")
      Just (b, _) -> pure b)

countSeqMatches :: forall n . (KnownNat n, n > 0) => Vector n Bit -> Vector n Bit -> Finite n
countSeqMatches xs ys
  = VS.zipWith (==) xs ys
 |> VS.elemIndex False
 |> fromMaybe maxFinite

maxFinite :: forall n . (KnownNat n, n >= 1) => Finite n
maxFinite = sNat @n |> toNatural |> toInteger |> subtract 1 |> finite
