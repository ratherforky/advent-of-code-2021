{-# LANGUAGE TypeOperators, DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE TypeApplications, ScopedTypeVariables #-}
{-# language NoImplicitPrelude, StrictData, OverloadedStrings, PatternSynonyms, MultiWayIf #-}
module Day5 where

import MyStreamingPrelude
import Util
import Parsing
import qualified Prelude as Prel

import qualified Streamly.Data.Fold as Fold
import qualified Streamly.Internal.Data.Fold as FoldI
import           Streamly.Internal.Data.Unfold (Unfold)
import qualified Streamly.Internal.Data.Unfold as UnfoldI
import qualified Streamly.Internal.Data.Parser as Parser


import Linear.V2
import Linear.Matrix hiding (trace)
import Linear.Vector
import Control.Lens.Combinators (set, view)
import Linear.Metric (norm)
import Linear (normalize, Metric (dot))
import Test.QuickCheck


-- import Data.Bimap (Bimap)
-- import qualified Data.Bimap as BM
-- import Control.Monad.Catch (throwM)

-- import qualified Streamly.Internal.Data.Array.Foreign.Mut as Arr
-- import Data.IORef
-- import Control.DeepSeq ( force, NFData )
-- import Control.Exception (evaluate)


--------------------------------------------------
-- Data types
--------------------------------------------------

data Line = Line Int Int Int Int
--               x1  y1  x2  y2
  deriving (Eq, Show)

data Equation
  = Vert Int
  -- x = (c)
  | Hori Int
  -- y = (c)
  | Diag Int    Int
  -- y = (m)x + (c)

--------------------------------------------------
-- Parsing and getting Input
--------------------------------------------------

parseDay5 :: Serial Line
parseDay5 = parseInputLines "src/inputs/day5.txt" day5Parser

parseDay5Eg :: Serial Line
parseDay5Eg = parseInputLines "src/inputs/day5-example.txt" day5Parser


day5Parser :: Parser IO Char Line
day5Parser = Line
  <$> int <* (char ',' <|> die "comma fail") <*> int
  <*  tok "->"
  <*> int <* (char ',' <|> die "comma fail") <*> int


--------------------------------------------------
-- Task 1
--------------------------------------------------

data VecRep = MkVR
  { point :: V2 Int
  , dir :: V2 Int
  } deriving (Show, Eq)

onLine :: Int -> Int -> Line -> Bool
onLine x y (Line x1 y1 x2 y2)
  = (x1 == x2 && x == x1 && between y1 y2 y) -- Vertical line
 || (y1 == y2 && y == y1 && between x1 x2 x) -- Horizontal line

between :: (Num a, Ord a) => a -> a -> a -> Bool
between bound1 bound2 x = minX <= x && x <= maxX
  where
    minX = min bound1 bound2
    maxX = max bound1 bound2

runTask1 :: IO Int
runTask1 = parseDay5Eg |> task1

task1 :: Serial Line -> IO Int
task1 = filter isHoriOrVerti
     .> trace print
     .> length
    --  .> countIntersections

isHoriOrVerti :: Line -> Bool
isHoriOrVerti (Line x1 y1 x2 y2)
  = x1 == x2 || y1 == y2

countIntersections :: Serial Line -> IO Int
countIntersections els
  = (do
      l1 <- els
      l2 <- els
      let l1VecRep = toVecRep l1
          l2VecRep = toVecRep l2
          maybeIntersect = intersection l1VecRep l2VecRep
      if | l1 == l2  -> nil -- Don't double count lines (breaks if there are multiple identical lines in the input)
         | sameAbsDirection (dir l1VecRep) (dir l2VecRep) -> sameLineCase l1VecRep l2VecRep
         | otherwise -> maybe nil pure (intersection (toVecRep l1) (toVecRep l2))
    )
  -- |> trace print
  |> uniq -- Make sure intersections are unique
  |> length

-- getIntPointsOnLine :: Monad m => VecRep -> Unfold m _ (V2 Int)
getIntPointsOnLine (MkVR p dir@(V2 dx dy)) = undefined
  where
    dirGcd = gcd dx dy
    dx' = dx `div` dirGcd
    dy' = dy `div` dirGcd

    -- foo :: Monad m => Unfold m ((Int,Int,Int),(Int,Int,Int))
    foo = zipWithDiffSeed V2 (UnfoldI.enumerateFromThenTo @Int) UnfoldI.enumerateFromThenTo
    intStepDir = V2 dx' dy'

zipWithDiffSeed :: Monad m => (c -> d -> e) -> Unfold m a c -> Unfold m b d -> Unfold m (a, b) e
zipWithDiffSeed = undefined

sameLineCase :: VecRep -> VecRep -> Serial (V2 Int)
sameLineCase l1@(MkVR p1 dir1) l2@(MkVR p2 dir2) = undefined
  -- = case dir1 of
  --     (V2 0 y) -> if sameFor _x then OnLine else Parallel -- Vertical dir
  --     (V2 x 0) -> if sameFor _y then OnLine else Parallel -- Horizontal dir
  --     (V2 x y) -> if r1 == r2   then OnLine else Parallel -- Diagonal dir

onVecLine :: VecRep -> V2 Int -> Bool
onVecLine (MkVR p1 dir1) p2 = r1 == r2 && r1 >= 0 && r1 <= 1
  where
    (V2 r1 r2) = findDirScalers p1 p2 dir1

data OnLineOrParallel = OnLine | Parallel
  deriving (Show, Eq)

onSameLine :: VecRep -> VecRep -> OnLineOrParallel
onSameLine (MkVR p1 dir1) (MkVR p2 _)
  = case dir1 of
      (V2 0 _) -> if sameFor _x then OnLine else Parallel
      (V2 _ 0) -> if sameFor _y then OnLine else Parallel
      (V2 _ _) -> if r1 == r2   then OnLine else Parallel
  where
    -- If both vals the same, then equations are consistent and on the same line
    (V2 r1 r2) = findDirScalers p1 p2 dir1

    sameFor lens = view lens p1 == view lens p2

findDirScalers :: V2 Int -> V2 Int -> V2 Int -> V2 (Ratio Int)
findDirScalers p1 p2 dir = fmap toRatio (p2 ^-^ p1) / fmap toRatio dir

-- Also true if exact opposite direction
sameAbsDirection :: V2 Int -> V2 Int -> Bool
sameAbsDirection dir1 dir2
  = abs dotResult == abs normMulResult
  where
    dotResult = dir1 `dot` dir2
    normMulResult = round (normDir dir1 * normDir dir2) -- Floating point eq, not ideal

    normDir :: V2 Int -> Float
    normDir = fmap fromIntegral .> norm

-- sameDirection :: VecRep -> VecRep -> Bool
-- sameDirection l1 l2
--   = dir l1 `dot` dir l2 == round (normDir l1 * normDir l2) -- Floating point eq, not ideal
--   -- || normDir1 == ((-1) *^ normDir2) -- Check oposite direction
--   where
--     normDir :: VecRep -> Float
--     normDir = dir .> fmap fromIntegral .> norm

-- Check if there are at least two lines which lie on this point
atLeastTwoIntersect :: Serial Line -> (Int, Int) -> IO Bool
atLeastTwoIntersect els (x,y)
  = els
 |> filter (onLine x y)
--  |> trace print
 |> drop 1
 |> head
 |> fmap isJust

boundingBox :: Monad m => Fold m Line ((Int, Int), (Int, Int))
boundingBox
  = Fold.foldl' (\((minX, minY), (maxX, maxY)) (Line x1 y1 x2 y2)
                    -> (( Prel.minimum [minX, x1, x2]
                        , Prel.maximum [maxX, x1, x2]),
                        ( Prel.minimum [minY, y1, y2]
                        , Prel.maximum [maxY, y1, y2]) ) )
                ((0, 0), (0, 0))

cells :: Monad m => Unfold m ((Int, Int), (Int, Int)) (Int, Int)
cells = UnfoldI.cross
          (UnfoldI.lmap fst rangeUnfold)
          (UnfoldI.lmap snd rangeUnfold)
  where
    rangeUnfold = UnfoldI.enumerateFromToIntegralBounded

-- Wrong
-- boundingBox :: Monad m => Fold m Line ((Int, Int), (Int, Int))
-- boundingBox
--   = FoldI.unzipWith (\(Line x1 y1 x2 y2) -> ((x1,y1), (x2,y2)))
--       (FoldI.unzipWith id foldMaximum' foldMaximum')
--       (FoldI.unzipWith id foldMaximum' foldMaximum')

foldMaximum', foldMinimum' :: Monad m => Fold m Int Int
foldMaximum' = Fold.maximum |> fmap fromJust
foldMinimum' = Fold.minimum |> fmap fromJust

-- REDO

toVecRep :: Line -> VecRep
toVecRep (Line x1 y1 x2 y2) = MkVR (V2 x1 y1) (V2 (x2 - x1) (y2 - y1))

-- >>> intersection v4 v5 == V2 2 5

intersection :: VecRep -> VecRep -> Maybe (V2 Int)
intersection (MkVR (V2 x1 y1) dir1@(V2 a b)) (MkVR (V2 x2 y2) dir2@(V2 c d))
  = do

    let bigA = V2 (V2 a (-c))
                  (V2 b (-d))

        target = V2 (x2 - x1) (y2 - y1)

        bigA1 = set (column _x) target bigA
        bigA2 = set (column _y) target bigA

        detA = det22 bigA

    r <- det22 bigA1 `safeRatio` detA
    s <- det22 bigA2 `safeRatio` detA

    -- For point to be on the lines, coeficents need to be between 0 and 1
    -- because direction vector takes up the full line
    guarded (between 0 1) r
    guarded (between 0 1) s

    let ratioPoint = r *^ fmap fromIntegral dir1  ^+^ s  *^ fmap fromIntegral dir2

    ratioPoint
      |> fmap extractInt
      |> sequenceA


safeRatio :: Int -> Int -> Maybe (Ratio Int)
safeRatio x y
  | y == 0    = Nothing
  | otherwise = Just $ x % y

extractInt :: Ratio Int -> Maybe Int
extractInt x
  = if denominator x == 1
      then Just (numerator x)
      else Nothing

-- Treat vectors as columns and put side by side into a matrix
colPack :: V2 a -> V2 a -> M22 a
colPack (V2 a b) (V2 c d)
  = V2 (V2 a c)
       (V2 b d)

cramer :: M22 Int -> V2 Int -> V2 (Ratio Int)
cramer bigA target = V2 alpha beta
  where

    alpha = det22 bigA1 % det22 bigA
    beta  = det22 bigA2 % det22 bigA

    bigA1 = set (column _x) target bigA
    bigA2 = set (column _y) target bigA

exactDiv :: Int -> Int -> Maybe Int
exactDiv _ 0 = Nothing -- Div by 0
exactDiv x y = case quotRem x y of
  (q, 0) -> Just q
  (_, _) -> Nothing

-- Task 1 testing

instance Arbitrary a => Arbitrary (V2 a) where
  arbitrary = liftA2 V2 arbitrary arbitrary

instance Arbitrary VecRep where
  arbitrary = liftA2 MkVR arbitrary arbitrary


prop_onSameLine_truepos :: VecRep -> Int -> Int -> Bool
prop_onSameLine_truepos l1 r k = onSameLine l1 l2 == OnLine
  where
    (MkVR p1 dir1) = l1
    l2 = MkVR (p1 + r *^ dir1) (k *^ dir1)

prop_onSameLine_trueneg :: VecRep -> V2 Int -> Int -> Int -> Property
prop_onSameLine_trueneg l1 offset r k
  =  Prel.all (/= 0) [r, k]
  && not (sameAbsDirection offset dir1)
 ==> onSameLine l1 l2 == Parallel
  where
    (MkVR p1 dir1) = l1
    l2 = MkVR (p1 + offset + r *^ dir1) (k *^ dir1)

multipleOf :: V2 Int -> V2 Int -> Bool
multipleOf = error "not implemented"

-- Failing test
vFailing = MkVR {point = V2 0 (-1), dir = V2 0 (-1)}
offsetFailing = V2 0 1
rFailing = 1
kFailing = 1

failure = onSameLine l1 l2
  where
    l1 = vFailing
    offset = offsetFailing
    r = rFailing
    k = kFailing
    (MkVR p1 dir1) = l1
    l2 = MkVR (p1 + offset + r *^ dir1) (k *^ dir1)

prop_exactDiv :: Int -> Int -> Bool
prop_exactDiv x 0 = exactDiv x 0 == Nothing
prop_exactDiv x y
  | (divResult * y) == x = exactDiv x y == Just divResult
  | otherwise            = exactDiv x y == Nothing
  where
    divResult = div x y

v1 = toVecRep (Line 0 9 5 9)
v2 = toVecRep (Line 8 0 0 8)
v3 = toVecRep (Line 9 4 3 4)
v4 = toVecRep (Line 0 5 4 5)
v5 = toVecRep (Line 2 0 2 7) -- intersection v4 v5 == Just (V2 2 5)
v6 = toVecRep (Line 2 0 2 4) -- intersection v4 v5 == Nothing
v7 = toVecRep (Line 5 0 5 4) -- intersection v4 v5 == Nothing

-- Same direction
v8 = toVecRep (Line 1 1 3 3)
v9 = toVecRep (Line 2 1 4 3)
v10 = toVecRep (Line 2 2 5 5)