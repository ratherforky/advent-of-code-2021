{-# language NoImplicitPrelude #-}
module Day1 where

import MyStreamingPrelude -- Based on Streamly rather than lists

import Util
import Parsing


--------------------------------------------------
-- Get Input
--------------------------------------------------

input :: Serial Int
input = parseInputLines
          "src/inputs/day1.txt"
          (entirely int <|> die "Failed to parse int")
-- input = unsafeReadInputLines "src/inputs/day1.txt"

--------------------------------------------------
-- Task 1
--------------------------------------------------

runTask1 :: IO Int
runTask1 = input |> task1

task1 :: Monad m => SerialT m Int -> m Int
task1 depths
  = zipWith compare (drop 1 depths) depths
 |> filter (== GT)
 |> length

--------------------------------------------------
-- Task 2
--------------------------------------------------

slidingWindowSum :: Monad m => SerialT m Int -> SerialT m Int
slidingWindowSum xs
  = xs
 |> zipWith (+) (drop 1 xs)
 |> zipWith (+) (drop 2 xs)

runTask2 :: IO Int
runTask2
  = input
 |> slidingWindowSum
 |> task1
