{-# language FlexibleContexts #-}
module MyStreamingPrelude
  ( module MyStreamingPrelude
  , module Flow
  , module Streamly.Prelude
  , module Data.Char
  , module System.IO
  , module Data.Finite
  -- , module Unicode
  , Control.Monad.IO.Class.MonadIO(..)
  , Reduce.parseMany
  , Reduce.splitOnSeq

  -- The following is copied from RIO.Prelude

    -- * @Bool@
    -- | Re-exported from "Data.Bool":
  , (Data.Bool.||)
  , (Data.Bool.&&)
  , Data.Bool.not
  , Data.Bool.otherwise
  , Data.Bool.bool

    -- * @Maybe@
    -- | Re-exported from "Data.Maybe":
  , Data.Maybe.maybe
  , Data.Maybe.fromMaybe
  , Data.Maybe.fromJust
  -- , RIO.Prelude.Extra.fromFirst
  , Data.Maybe.isJust
  , Data.Maybe.isNothing
  , Data.Maybe.listToMaybe
  , Data.Maybe.maybeToList
  , Data.Maybe.catMaybes
  -- , Data.Maybe.mapMaybe
  -- , RIO.Prelude.Extra.mapMaybeA
  -- , RIO.Prelude.Extra.mapMaybeM
  -- , RIO.Prelude.Extra.forMaybeA
  -- , RIO.Prelude.Extra.forMaybeM

    -- * @Either@
    -- | Re-exported from "Data.Either":
  , Data.Either.either
  , Data.Either.fromLeft
  , Data.Either.fromRight
  , Data.Either.isLeft
  , Data.Either.isRight
  -- , RIO.Prelude.Extra.mapLeft
  , Data.Either.lefts
  , Data.Either.partitionEithers
  , Data.Either.rights

    -- * Tuples
    -- | Re-exported from "Data.Tuple":
  , Data.Tuple.fst
  , Data.Tuple.snd
  , Data.Tuple.curry
  , Data.Tuple.uncurry
  -- , Data.Tuple.swap -- TODO: export?

    -- * @Eq@
    -- | Re-exported from "Data.Eq":
  , (Data.Eq.==)
  , (Data.Eq./=)

    -- * @Ord@
    -- | Re-exported from "Data.Ord":
  , (Data.Ord.<)
  , (Data.Ord.<=)
  , (Data.Ord.>)
  , (Data.Ord.>=)
  , Data.Ord.max
  , Data.Ord.min
  , Data.Ord.compare
  , Data.Ord.comparing
  , Data.Ord.Down(..)

    -- * @Enum@
    -- | Re-exported from "Prelude":
  , Prelude.fromEnum

    -- * @Bounded@
    -- | Re-exported from "Prelude":
  , Prelude.minBound
  , Prelude.maxBound

    -- * @Num@
    -- | Re-exported from "Prelude":
  , (Prelude.+)
  , (Prelude.-)
  , (*) -- HSE can't parse qualified export, which results in hlint error.
  , (Prelude.^)
  , Prelude.negate
  , Prelude.abs
  , Prelude.signum
  , Prelude.fromInteger
  , Prelude.subtract

    -- * @Real@
    -- | Re-exported from "Prelude":
  , Prelude.toRational

    -- * @Integral@
    -- | Re-exported from "Prelude":
  , Prelude.quot
  , Prelude.rem
  , Prelude.div
  , Prelude.mod
  , Prelude.quotRem
  , Prelude.divMod
  , Prelude.toInteger
  , Prelude.even
  , Prelude.odd
  , Prelude.gcd
  , Prelude.lcm
  , Prelude.fromIntegral

    -- * @Fractional@
    -- | Re-exported from "Prelude":
  , (Prelude./)
  , (Prelude.^^)
  , Prelude.recip
  , Prelude.fromRational
  , Prelude.realToFrac

    -- * @Floating@
    -- | Re-exported from "Prelude":
  , Prelude.pi
  , Prelude.exp
  , Prelude.log
  , Prelude.sqrt
  , (Prelude.**)
  , Prelude.logBase
  , Prelude.sin
  , Prelude.cos
  , Prelude.tan
  , Prelude.asin
  , Prelude.acos
  , Prelude.atan
  , Prelude.sinh
  , Prelude.cosh
  , Prelude.tanh
  , Prelude.asinh
  , Prelude.acosh
  , Prelude.atanh

    -- * @RealFrac@
    -- | Re-exported from "Prelude":
  , Prelude.properFraction
  , Prelude.truncate
  , Prelude.round
  , Prelude.ceiling
  , Prelude.floor

    -- * @RealFloat@
    -- | Re-exported from "Prelude":
  , Prelude.floatRadix
  , Prelude.floatDigits
  , Prelude.floatRange
  , Prelude.decodeFloat
  , Prelude.encodeFloat
  , Prelude.exponent
  , Prelude.significand
  , Prelude.scaleFloat
  , Prelude.isNaN
  , Prelude.isInfinite
  , Prelude.isDenormalized
  , Prelude.isNegativeZero
  , Prelude.isIEEE
  , Prelude.atan2

    -- * @Word@
    -- | Re-exported from "Data.Word":
  , Data.Word.byteSwap16
  , Data.Word.byteSwap32
  , Data.Word.byteSwap64

    -- * @Semigroup@
    -- | Re-exported from "Data.Semigroup":
  , (Data.Semigroup.<>)
  -- , RIO.Prelude.Renames.sappend

    -- * @Monoid@
    -- | Re-exported from "Data.Monoid":
  , Data.Monoid.mempty
  , Data.Monoid.mappend
  , Data.Monoid.mconcat

    -- * @Functor@
    -- | Re-exported from "Data.Functor":
  , Data.Functor.fmap
  , (Data.Functor.<&>)
  , (Data.Functor.<$>)
  , (Data.Functor.<$)
  , (Data.Functor.$>)
  , Data.Functor.void
  -- , (RIO.Prelude.Extra.<&>)

    -- * @Applicative@
    -- | Re-exported from "Control.Applicative":
  , Control.Applicative.pure
  , (Control.Applicative.<*>)
  , (Control.Applicative.<*)
  , (Control.Applicative.*>)
  , Control.Applicative.liftA
  , Control.Applicative.liftA2
  , Control.Applicative.liftA3
  , Control.Monad.forever
  , Data.Foldable.traverse_
  , Data.Foldable.for_
  , Data.Foldable.sequenceA_
  -- , Control.Monad.filterM
  -- , Control.Monad.replicateM_
  -- , Control.Monad.zipWithM
  -- , Control.Monad.zipWithM_

    -- * @Monad@
    -- | Re-exported from "Control.Monad":
  , Control.Monad.return
  , Control.Monad.join
  , Control.Monad.fail
  , (Control.Monad.>>=)
  , (Control.Monad.>>)
  , (Control.Monad.=<<)
  , (Control.Monad.>=>)
  , (Control.Monad.<=<)
  , (Control.Monad.<$!>)
  , Control.Monad.liftM
  , Control.Monad.liftM2
  -- , RIO.Prelude.Extra.whenM
  -- , RIO.Prelude.Extra.unlessM
  -- , Data.Foldable.mapM_
  -- , Data.Foldable.forM_
  -- , Data.Foldable.sequence_

  -- , Control.Monad.foldM
  -- , Control.Monad.foldM_

  --   -- * @Foldable@
  --   -- | Re-exported from "Data.Foldable":
  -- , Data.Foldable.foldr
  -- , Data.Foldable.foldl'
  -- , Data.Foldable.fold
  -- , Data.Foldable.foldMap
  -- , RIO.Prelude.Extra.foldMapM
  -- , Data.Foldable.elem
  -- , Data.Foldable.notElem
  -- , Data.Foldable.null
  -- , Data.Foldable.length
  -- , Data.Foldable.sum
  -- , Data.Foldable.product
  -- , Data.Foldable.all
  -- , Data.Foldable.any
  -- , Data.Foldable.and
  -- , Data.Foldable.or
  -- , Data.Foldable.toList
  -- , Data.Foldable.concat
  -- , Data.Foldable.concatMap

    -- * @Traversable@
    -- | Re-exported from "Data.Traversable":
  , Data.Traversable.traverse
  , Data.Traversable.for
  , Data.Traversable.sequenceA
  -- , Data.Traversable.mapM
  -- , Data.Traversable.forM
  -- , Data.Traversable.sequence

    -- * @Alternative@
    -- | Re-exported from "Control.Applicative":
  , (Control.Applicative.<|>)
  -- , Control.Applicative.some
  -- , Control.Applicative.many
  , Control.Applicative.optional
  , Data.Foldable.asum
  , Control.Monad.guard
  , Control.Monad.when
  , Control.Monad.unless

    -- * @Bifunctor@
    -- | Re-exported from "Data.Bifunctor":
  , Data.Bifunctor.bimap
  , Data.Bifunctor.first
  , Data.Bifunctor.second

    -- * @Bifoldable@
    -- | Re-exported from "Data.Bifoldable":
  , Data.Bifoldable.bifold
  , Data.Bifoldable.bifoldMap
  , Data.Bifoldable.bifoldr
  , Data.Bifoldable.bifoldl
  , Data.Bifoldable.bifoldr'
  , Data.Bifoldable.bifoldr1
  , Data.Bifoldable.bifoldrM
  , Data.Bifoldable.bifoldl'
  , Data.Bifoldable.bifoldl1
  , Data.Bifoldable.bifoldlM
  , Data.Bifoldable.bitraverse_
  , Data.Bifoldable.bifor_
  , Data.Bifoldable.bisequence_
  , Data.Bifoldable.biasum
  , Data.Bifoldable.biList
  , Data.Bifoldable.binull
  , Data.Bifoldable.bilength
  , Data.Bifoldable.bielem
  , Data.Bifoldable.bimaximum
  , Data.Bifoldable.biminimum
  , Data.Bifoldable.bisum
  , Data.Bifoldable.biproduct
  , Data.Bifoldable.biconcat
  , Data.Bifoldable.biconcatMap
  , Data.Bifoldable.biand
  , Data.Bifoldable.bior
  , Data.Bifoldable.biany
  , Data.Bifoldable.biall
  , Data.Bifoldable.bimaximumBy
  , Data.Bifoldable.biminimumBy
  , Data.Bifoldable.binotElem
  , Data.Bifoldable.bifind

    -- * @Bitraverse@
    -- | Re-exported from "Data.Bitraversable":
  , Data.Bitraversable.bitraverse
  , Data.Bitraversable.bisequence
  , Data.Bitraversable.bifor
  , Data.Bitraversable.bimapAccumL
  , Data.Bitraversable.bimapAccumR

    -- * @MonadPlus@
    -- | Re-exported from "Control.Monad":
  , Control.Monad.mzero
  , Control.Monad.mplus
  , Control.Monad.msum
  , Control.Monad.mfilter

    -- * @Arrow@
    -- | Re-exported from "Control.Arrow" and "Control.Category":
  , (Control.Arrow.&&&)
  , (Control.Arrow.***)
  , (Control.Category.>>>)

    -- * @Function@
    -- | Re-exported from "Data.Function":
  , Data.Function.id
  , Data.Function.const
  , (Data.Function..)
  , (Data.Function.$)
  , (Data.Function.&)
  , Data.Function.flip
  , Data.Function.fix
  , Data.Function.on

    -- * Miscellaneous functions
  , (Prelude.$!)
  , Prelude.seq
  , Prelude.error
  , Prelude.undefined
  , Prelude.asTypeOf
  -- , RIO.Prelude.Extra.asIO

  --   -- * List
  --   -- | Re-exported from "Data.List":
  , (Data.List.++)
  -- , Data.List.break
  -- , Data.List.drop
  -- , Data.List.dropWhile
  -- , Data.List.filter
  -- , Data.List.lookup
  -- , Data.List.map
  -- , Data.List.replicate
  -- , Data.List.reverse
  -- , Data.List.span
  -- , Data.List.take
  -- , Data.List.takeWhile
  -- , Data.List.zip
  -- , Data.List.zipWith
  -- , RIO.Prelude.Extra.nubOrd


  --   -- * @String@
  --   -- | Re-exported from "Data.String":
  -- , Data.String.fromString
  -- , Data.String.lines
  -- , Data.String.unlines
  -- , Data.String.unwords
  -- , Data.String.words

    -- ** @Show@
    -- | Re-exported from "Text.Show":
  , Text.Show.show
    -- ** @Read@
    -- | Re-exported from "Text.Read":
  , Text.Read.readMaybe

    -- * @NFData@

  --   -- | Re-exported from "Control.DeepSeq":
  -- , (Control.DeepSeq.$!!)
  -- , Control.DeepSeq.rnf
  -- , Control.DeepSeq.deepseq
  -- , Control.DeepSeq.force

    -- * @Void@
    -- | Re-exported from "Data.Void":
  , Data.Void.absurd

    -- * @Reader@
    -- | Re-exported from "Control.Monad.Reader":
  , Control.Monad.Reader.lift
  , Control.Monad.Reader.ask
  , Control.Monad.Reader.asks
  , Control.Monad.Reader.local
  , Control.Monad.Reader.runReader
  -- , Control.Monad.Reader.runReaderT

  --   -- * @ByteString@
  --   -- | Helper synonyms for converting bewteen lazy and strict @ByteString@s
  -- , RIO.Prelude.Renames.toStrictBytes
  -- , RIO.Prelude.Renames.fromStrictBytes

  --   -- * @ShortByteString@
  --   -- | Re-exported from "Data.ByteString.Short":
  -- , Data.ByteString.Short.toShort
  -- , Data.ByteString.Short.fromShort

  --   -- * @Text@
  -- , RIO.Prelude.Text.tshow
  -- , RIO.Prelude.Text.decodeUtf8Lenient
  --   -- | Re-exported from "Data.Text.Encoding":
  -- , Data.Text.Encoding.decodeUtf8'
  -- , Data.Text.Encoding.decodeUtf8With
  -- , Data.Text.Encoding.encodeUtf8
  -- , Data.Text.Encoding.encodeUtf8Builder
  -- , Data.Text.Encoding.Error.lenientDecode

  --   -- * @PrimMonad@
  --   -- | Re-exported from "Control.Monad.Primitive":
  -- , Control.Monad.Primitive.primitive
    -- | Re-exported from "Control.Monad.ST":
  , Control.Monad.ST.runST

  -- Types (mostly copied from RIO)

     -- * @base@
    -- ** Types
    -- *** @Bool@
    -- | Re-exported from "Data.Bool":
  ,  Data.Bool.Bool(..)
    -- | Re-exported from "Data.String":
  , Data.String.String
    -- *** @Ordering@
    -- | Re-exported from "Data.Ord":
  , Data.Ord.Ordering(..)
    -- *** Numbers
    -- **** @Int@
    -- | Re-exported from "Data.Int":
  , Data.Int.Int
    -- ***** @Int8@
  , Data.Int.Int8
    -- ***** @Int16@
  , Data.Int.Int16
    -- ***** @Int32@
  , Data.Int.Int32
    -- ***** @Int64@
  , Data.Int.Int64
    -- **** @Word@
    -- | Re-exported from "Data.Word":
  , Data.Word.Word
    -- ***** @Word8@
  , Data.Word.Word8
    -- ***** @Word16@
  , Data.Word.Word16
    -- ***** @Word32@
  , Data.Word.Word32
    -- ***** @Word64@
  , Data.Word.Word64
    -- **** @Integer@
    -- | Re-exported from "Prelude.Integer":
  , Prelude.Integer
    -- **** @Natural@
    -- | Re-exported from "Numeric.Natural":
  , Numeric.Natural.Natural
    -- **** @Rational@
    -- | Re-exported from "Data.Ratio":
  , Data.Ratio.Rational
  , Data.Ratio.Ratio
  , (Data.Ratio.%)
  , Data.Ratio.denominator
  , Data.Ratio.numerator
    -- **** @Float@
    -- | Re-exported from "Prelude":
  , Prelude.Float
    -- **** @Double@
    -- | Re-exported from "Prelude":
  , Prelude.Double

    -- *** @Maybe@
    -- | Re-exported from "Data.Maybe":
  , Data.Maybe.Maybe(..)
    -- *** @Either@
    -- | Re-exported from "Data.Either":
  , Data.Either.Either(..)
    -- *** @NonEmpty@
    -- | Re-exported from Data.List.NonEmpty
  , Data.List.NonEmpty.NonEmpty(..)
    -- *** @Proxy@
    -- | Re-exported from "Data.Proxy":
  , Data.Proxy.Proxy(..)
    -- *** @Void@
    -- | Re-exported from "Data.Void":
  , Data.Void.Void
    -- *** @Const@
    -- | Re-exported from "Data.Functor.Const":
  , Data.Functor.Const.Const(..)
    -- *** @Identity@
    -- | Re-exported from "Data.Functor.Identity":
  , Data.Functor.Identity.Identity(..)
    -- *** @ST@
    -- | Re-exported from "Control.Monad.ST":
  , Control.Monad.ST.ST

    -- ** Type Classes

    -- *** @Eq@
    -- | Re-exported from "Data.Eq":
  , Data.Eq.Eq

    -- *** @Ord@
    -- | Re-exported from "Data.Ord":
  , Data.Ord.Ord

    -- *** @Bounded@
    -- | Re-exported from "Prelude":
  , Prelude.Bounded

    -- *** @Enum@
    -- | Re-exported from "Prelude":
  , Prelude.Enum

    -- *** Strings
    -- **** @Show@
    -- | Re-exported from "Text.Show":
  , Text.Show.Show
    -- **** @Read@
    -- | Re-exported from "Text.Read":
  , Text.Read.Read
    -- **** @IsString@
    -- | Re-exported from "Data.String":
  , Data.String.IsString

    -- *** Numeric
    -- | All numeric classes are re-exported from "Prelude":

    -- **** @Num@
  , Prelude.Num
    -- **** @Fractional@
  , Prelude.Fractional
    -- **** @Floating@
  , Prelude.Floating
    -- **** @Real@
  , Prelude.Real
    -- **** @Integral@
  , Prelude.Integral
    -- **** @RealFrac@
  , Prelude.RealFrac
    -- **** @RealFloat@
  , Prelude.RealFloat

    -- *** Categories
    -- **** @Functor@
    -- | Re-exported from "Data.Functor":
  , Data.Functor.Functor
    -- **** @Bifunctor@
    -- | Re-exported from "Data.Bifunctor":
  , Data.Bifunctor.Bifunctor
    -- **** @Foldable@
    -- | Re-exported from "Data.Foldable":
  , Data.Foldable.Foldable
    -- **** @Bifoldable@
    -- | Re-exported from "Data.Bifoldable":
  , Data.Bifoldable.Bifoldable
    -- **** @Semigroup@
    -- | Re-exported from "Data.Semigroup":
  , Data.Semigroup.Semigroup
    -- **** @Monoid@
    -- | Re-exported from "Data.Monoid":
  , Data.Monoid.Monoid
    -- **** @Applicative@
    -- | Re-exported from "Control.Applicative":
  , Control.Applicative.Applicative
    -- **** @Alternative@
    -- | Re-exported from "Control.Applicative":
  , Control.Applicative.Alternative
    -- **** @Traversable@
    -- | Re-exported from "Data.Traversable":
  , Data.Traversable.Traversable
    -- **** @Bitraversable@
    -- | Re-exported from "Data.Bitraversable":
  , Data.Bitraversable.Bitraversable
    -- **** @Monad@
    -- | Re-exported from "Control.Monad":
  , Control.Monad.Monad
    -- **** @MonadPlus@
    -- | Re-exported from "Control.Monad":
  , Control.Monad.MonadPlus
    -- **** @Category@
    -- | Re-exported from "Control.Category":
  , Control.Category.Category
    -- **** @Arrow@
    -- | Re-exported from "Control.Arrow":
  , Control.Arrow.Arrow
    -- **** @MonadFail@
    -- | Re-exported from "Control.Monad.Fail":
  , Control.Monad.Fail.MonadFail

    -- *** Data
    -- **** @Typeable@
    -- | Re-exported from "Control.Monad":
  , Data.Typeable.Typeable
    -- **** @Data@
    -- | Re-exported from "Data.Data":
  , Data.Data.Data(..)
    -- **** @Generic@
    -- | Re-exported from "GHC.Generics":
  , GHC.Generics.Generic
    -- **** @Storable@
    -- | Re-exported from "Foreign.Storable":
  , Foreign.Storable.Storable

    -- *** Exceptions
    -- *** @Exception@
    -- | Re-exported from "Control.Exception.Base":
  , Control.Exception.Base.Exception
    -- **** @HasCallStack@
    -- | Re-exported from "GHC.Stack":
  , GHC.Stack.HasCallStack


  --   -- * @deepseq@
  --   -- ** @NFData@
  --   -- | Re-exported from "Control.DeepSeq":
  -- , Control.DeepSeq.NFData

    -- * @mtl@
    -- ** @MonadTrans@
    -- | Re-exported from "Control.Monad.Reader":
  , Control.Monad.Reader.MonadTrans
    -- ** @MonadReader@
  , Control.Monad.Reader.MonadReader
    -- ** @ReaderT@ (@Reader@)
    -- | Re-exported from "Control.Monad.Reader":
  , Control.Monad.Reader.Reader
  , Control.Monad.Reader.ReaderT(ReaderT)

    -- * @exceptions@
    -- ** @MonadThrow@
    -- ** @MonadCatch@
    -- | Re-exported from "Control.Monad.Catch":
  , Control.Monad.Catch.MonadThrow
  , Control.Monad.Catch.MonadCatch

  --   -- * @bytestring@
  --   -- ** @ByteString@
  --   -- | Re-exported from "Data.ByteString":
  -- , Data.ByteString.ByteString
  --   -- ** @LByteString@
  --   -- | A synonym for lazy `Data.ByteString.Lazy.ByteString` re-exported from "Data.ByteString.Lazy":
  -- , RIO.Prelude.Renames.LByteString
  --   -- ** @Builder@
  --   -- | Re-exported from "Data.ByteString.Builder":
  -- , Data.ByteString.Builder.Builder
  --   -- ** @ShortByteString@
  --   -- | Re-exported from "Data.ByteString.Short":
  -- , Data.ByteString.Short.ShortByteString

    -- * @text@
    -- ** @Text@
    -- | Re-exported from "Data.Text":
  -- , Data.Text.Text
  --   -- ** @LText@
  --   -- | A synonym for lazy `Data.Text.Lazy.Text` re-exported from "Data.Text.Lazy":
  -- , RIO.Prelude.Renames.LText
    -- ** @UncodeException@
    -- | Re-exported from "Data.Text.Encoding.Error":
  -- , Data.Text.Encoding.Error.UnicodeException(..)

  --   -- * @vector@
  --   -- ** @Vector@
  --   -- | Boxed vector re-exported from "Data.Vector":
  -- , Data.Vector.Vector
  --   -- ** @UVector@
  --   -- | A synonym for unboxed `Data.Vector.Unboxed.Vector` re-exported from "Data.Vector.Unboxed":
  -- , RIO.Prelude.Renames.UVector
  --   -- *** @Unbox@
  -- , Data.Vector.Unboxed.Unbox
  --   -- ** @SVector@
  --   -- | A synonym for storable `Data.Vector.Storable.Vector` re-exported from "Data.Vector.Storable":
  -- , RIO.Prelude.Renames.SVector
  --   -- ** @GVector@
  --   -- | A synonym for generic `Data.Vector.Generic.Vector` re-exported from "Data.Vector.Generic":
  -- , RIO.Prelude.Renames.GVector

    -- * @containers@
    -- ** @IntMap@
    -- | Re-exported from "Data.IntMap.Strict":
  , Data.IntMap.Strict.IntMap
    -- ** @Map@
    -- | Re-exported from "Data.Map.Strict":
  , Data.Map.Strict.Map
    -- ** @IntSet@
    -- | Re-exported from "Data.IntSet":
  , Data.IntSet.IntSet
    -- ** @Set@
    -- | Re-exported from "Data.Set":
  , Data.Set.Set
  --   -- ** @Seq@
  --   -- | Re-exported from "Data.Seq":
  -- , Data.Sequence.Seq

    -- * @hashable@
    -- ** @Hashable@
  , Data.Hashable.Hashable

    -- * @unordered-containers@
    -- ** @HashMap@
    -- | Re-exported from "Data.HashMap.Strict":
  , Data.HashMap.Strict.HashMap
    -- ** @HashSet@
    -- | Re-exported from "Data.HashSet":
  , Data.HashSet.HashSet

  --   -- * @primitive@
  --   -- ** @PrimMonad@
  --   -- | Re-exported from "Control.Monad.Primitive":
  -- , Control.Monad.Primitive.PrimMonad (PrimState)
  
  -- My additions

  , Streamly.Data.Fold.Fold

  ) where

import Flow
import Streamly.Prelude
import Streamly.Prelude as Stream
import Data.Char
import System.IO
import Data.Finite
-- import qualified Prelude
import qualified Streamly.Internal.Data.Stream.IsStream.Reduce as Reduce

import Streamly.Internal.Unicode.Stream as Unicode

-- imports copied from RIO

import qualified Control.Applicative
import qualified Control.Arrow
import qualified Control.Category
-- import qualified Control.DeepSeq
import qualified Control.Monad
-- import qualified Control.Monad.Primitive (primitive)
import qualified Control.Monad.Reader
import qualified Control.Monad.ST
import qualified Control.Monad.IO.Class
import qualified Data.Bifoldable
import qualified Data.Bifunctor
import qualified Data.Bitraversable
import qualified Data.Bool
-- import qualified Data.ByteString.Short
import qualified Data.Either
import qualified Data.Eq
import qualified Data.Foldable
import qualified Data.Function
import qualified Data.Functor
import qualified Data.List
import qualified Data.Maybe
import qualified Data.Monoid
import qualified Data.Ord
import qualified Data.Semigroup
import qualified Data.String
-- import qualified Data.Text.Encoding
--     (decodeUtf8', decodeUtf8With, encodeUtf8, encodeUtf8Builder)
import qualified Data.Text.Encoding.Error (lenientDecode)
import qualified Data.Traversable
import qualified Data.Tuple
import qualified Data.Void
import qualified Data.Word
import           Prelude ((*))
import qualified Prelude
import qualified Text.Read
import qualified Text.Show


-- Type imports
import qualified Control.Applicative
import qualified Control.Arrow
import qualified Control.Category
-- import qualified Control.DeepSeq
import qualified Control.Exception.Base
import qualified Control.Monad
import qualified Control.Monad.Catch
import qualified Control.Monad.Fail
-- import qualified Control.Monad.Primitive (PrimMonad(..))
import qualified Control.Monad.Reader
import qualified Control.Monad.ST
import qualified Data.Bifoldable
import qualified Data.Bifunctor
import qualified Data.Bitraversable
import qualified Data.Bool
-- import qualified Data.ByteString (ByteString)
-- import qualified Data.ByteString.Builder (Builder)
-- import qualified Data.ByteString.Short
import qualified Data.Char
import qualified Data.Data
import qualified Data.Either
import qualified Data.Eq
import qualified Data.Foldable
import qualified Data.Function
import qualified Data.Functor
import qualified Data.Functor.Const
import qualified Data.Functor.Identity
import qualified Data.Hashable
import qualified Data.HashMap.Strict
import qualified Data.HashSet
import qualified Data.Int
import qualified Data.IntMap.Strict
import qualified Data.IntSet
import qualified Data.List
import qualified Data.List.NonEmpty
import qualified Data.Map.Strict
import qualified Data.Maybe
import qualified Data.Monoid (Monoid)
import qualified Data.Ord
import qualified Data.Proxy
import qualified Data.Ratio
import qualified Data.Semigroup (Semigroup)
import qualified Data.Sequence
import qualified Data.Set
import qualified Data.String (IsString, String)
-- import qualified Data.Text (Text)
-- import qualified Data.Text.Encoding.Error
import qualified Data.Traversable
import qualified Data.Typeable
-- import qualified Data.Vector
-- import qualified Data.Vector.Unboxed (Unbox)
import qualified Data.Void
import qualified Data.Word
import qualified Foreign.Storable
import qualified GHC.Generics
import qualified GHC.Stack
import qualified Numeric.Natural
import qualified Prelude
import qualified System.Exit
import qualified System.IO
-- import qualified Text.Read
-- import qualified Text.Show
import qualified Streamly.Data.Fold
