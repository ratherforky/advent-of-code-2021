{-# LANGUAGE TypeOperators, DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE TypeApplications, ScopedTypeVariables #-}
{-# language NoImplicitPrelude, StrictData, OverloadedStrings, PatternSynonyms #-}
module Day4 where

import MyStreamingPrelude
import Util
import Parsing
import qualified Prelude as Prel

import qualified Streamly.Data.Fold as Fold
import qualified Streamly.Internal.Data.Fold as FoldI
import qualified Streamly.Internal.Data.Parser as Parser

import Data.Bimap (Bimap)
import qualified Data.Bimap as BM
import Control.Monad.Catch (throwM)

import qualified Streamly.Internal.Data.Array.Foreign.Mut as Arr
import Data.IORef
import Control.DeepSeq ( force, NFData )
import Control.Exception (evaluate)


--------------------------------------------------
-- Data types
--------------------------------------------------

-- Starts top left
-- data Index = (Finite 5) :@: (Finite 5)
--   deriving (Eq, Show)
newtype Index = MkI ((Finite 5) :+: (Finite 5))
  deriving stock (Eq, Show)
  -- deriving Ord via Flip ((Finite 5) :+: (Finite 5))
  -- deriving Show via (Finite 5) :+: (Finite 5)

instance Ord Index where
  compare (MkI (x1 :+: y1)) (MkI (x2 :+: y2)) = compare (y1 :+: x1) (y2 :+: x2)

-- newtype Flip a = MkFlip (a :+: a)
--   deriving Eq

-- instance Ord a => Ord (Flip a) where
--   compare (MkFlip (x1 :+: y1)) (MkFlip (x2 :+: y2)) = compare (MkFlip (y1 :+: x1)) (MkFlip (y2 :+: x2))


pattern x :@: y = MkI (x :+: y)

{-# COMPLETE (:@:) #-}

infixr 5 :@:

-- Error on (4 :+: 4)
advanceI :: Index -> Maybe Index
advanceI (x :@: y)
  | x == 4 && y == 4 = Nothing
  | x == 4    = Just $ 0   :@: y+1
  | otherwise = Just $ x+1 :@: y

data BingoNum = Marked Int | Unmarked Int
  deriving (Eq, Ord, Show)

isMarked :: BingoNum -> Bool
isMarked (Marked _) = True
isMarked _ = False

markNum :: BingoNum -> BingoNum
markNum (Unmarked x) = Marked x
markNum x = x

type BingoCard = Bimap Index BingoNum

-- Terminating fold, ends once bingo card is full
toBingoCard :: Monad m => Fold m Int BingoCard
toBingoCard = FoldI.mkFold step (FoldI.Partial (BM.empty :+: (0 :@: 0))) fst'
  where
    step :: BingoCard :+: Index -> Int -> FoldI.Step (BingoCard :+: Index) BingoCard
    step (bingoCard :+: i) x
      = case advanceI i of
          Nothing -> FoldI.Done    (BM.insert i (Unmarked x) bingoCard)
          Just i' -> FoldI.Partial (BM.insert i (Unmarked x) bingoCard :+: i')



--------------------------------------------------
-- Parsing and getting Input
--------------------------------------------------

rawInputEg :: Serial Char
rawInputEg = readChars "src/inputs/day4-example.txt"

rawInput :: Serial Char
rawInput = readChars "src/inputs/day4.txt"


parseDay4 :: Serial Char -> Serial Int :+: Serial BingoCard
parseDay4 xs = numbers :+: bingoCards
  where
    numbers
      = takeWhile (/= '\n') xs
     |> splitOn (== ',') (toFold int)

    bingoCards
      = dropWhile (/= '\n') xs
     |> dropWhile (not . isDigit)
     |> splitOnSeq "\n\n" (FoldI.toStream)
    --  |> map (trace print)
     |> fromSerial
     |> mapM (parse $ Parser.many (whitespace *> int <* whitespace) toBingoCard)
     |> fromSerial -- May or may not be a good idea to Async
    --  |> trace print

exampleBingo :: BingoCard
exampleBingo = BM.fromList [(MkI (finite 0 :+: finite 0),Unmarked 22),(MkI (finite 1 :+: finite 0),Unmarked 13),(MkI (finite 2 :+: finite 0),Unmarked 17),(MkI (finite 3 :+: finite 0),Unmarked 11),(MkI (finite 4 :+: finite 0),Unmarked 0),(MkI (finite 0 :+: finite 1),Unmarked 8),(MkI (finite 1 :+: finite 1),Unmarked 2),(MkI (finite 2 :+: finite 1),Unmarked 23),(MkI (finite 3 :+: finite 1),Unmarked 4),(MkI (finite 4 :+: finite 1),Unmarked 24),(MkI (finite 0 :+: finite 2),Unmarked 21),(MkI (finite 1 :+: finite 2),Unmarked 9),(MkI (finite 2 :+: finite 2),Unmarked 14),(MkI (finite 3 :+: finite 2),Unmarked 16),(MkI (finite 4 :+: finite 2),Unmarked 7),(MkI (finite 0 :+: finite 3),Unmarked 6),(MkI (finite 1 :+: finite 3),Unmarked 10),(MkI (finite 2 :+: finite 3),Unmarked 3),(MkI (finite 3 :+: finite 3),Unmarked 18),(MkI (finite 4 :+: finite 3),Unmarked 5),(MkI (finite 0 :+: finite 4),Unmarked 1),(MkI (finite 1 :+: finite 4),Unmarked 12),(MkI (finite 2 :+: finite 4),Unmarked 20),(MkI (finite 3 :+: finite 4),Unmarked 15),(MkI (finite 4 :+: finite 4),Unmarked 19)]

--------------------------------------------------
-- Task 1
--------------------------------------------------

runTask1 :: IO Int
runTask1
  = rawInput |> parseDay4 |> task1

task1 :: Serial Int :+: Serial BingoCard -> IO Int
task1 = winningBoardScores
     .> find isJust
     .> fmap (\(Just (Just x)) -> x)

winningBoardScores :: Serial Int :+: Serial BingoCard -> Serial (Maybe Int) 
winningBoardScores (numbers :+: initBingoCards) = do
  bingoCardsRef <- liftIO $ newIORef $ initBingoCards
  x <- numbers

  liftIO $ atomicModifyIORef' bingoCardsRef $ \bingoCards ->
      (bingoCards
        |> fromSerial
        |> mapM (markCard x .> evaluate) -- Hopefully evaluation happens in the threads 
        |> fromSerial, ()) -- Async actually makes this slower. Threaded + Serial is faster though
  
  fromEffect $ do
    bingoCards <- readIORef bingoCardsRef
    bingo <- bingoCards |> findM hasBingo

    case bingo of
      Just bi -> do
        liftIO $ atomicModifyIORef' bingoCardsRef (\bcs -> (filterM (fmap not . hasBingo) bcs, ()))
        BM.toAscList bi
          |> fromList
          -- |> trace print
          |> map snd
          |> filter (not . isMarked)
          |> map (\(Unmarked y) -> y)
          |> fold (Fold.sum <&> (* x))
          |> fmap Just
      Nothing -> pure Nothing

-- Not sure about strictness properties
-- processCard :: MonadIO m => Int -> BingoCard -> m (Bool :+: BingoCard)
-- processCard x card = hasBingo card' <&> (:+: card')
--   where
--     card' = markCard x card

hasBingo :: forall m . MonadIO m => BingoCard -> m Bool
hasBingo card = do
  arr <- Arr.fromListN @m @Int 10 (Prel.repeat 0)
  markedIndexes
    |> mapM_ (\(x :@: y) -> do
        modifyIndexUnsafe' arr (toInt x) (+ 1)
        modifyIndexUnsafe' arr (toInt y + 5) (+ 1)
      )
  
  unfold Arr.read arr
    |> any (== 5)
  where
    markedIndexes
      = BM.toAscList card
     |> fromList @m @SerialT
     |> filter (snd .> isMarked)
     |> map fst

markCard :: Int -> BingoCard -> BingoCard
markCard x = adjustRKey markNum (Unmarked x)

adjustRKey :: (Ord a, Ord b) => (b -> b) -> b -> Bimap a b -> Bimap a b
adjustRKey f rkey bmap
  = case rkey `BM.lookupR` bmap of
      Nothing   -> bmap -- If key not in Bimap, leave unchanged
      Just lkey -> BM.adjust f lkey bmap

modifyIndexUnsafe' :: (MonadIO m, Storable a) => Arr.Array a -> Int -> (a -> a) -> m ()
modifyIndexUnsafe' arr i f = Arr.modifyIndexUnsafe arr i (\x -> (f x, ()))

-- bin :: Index -> [Int]
-- bin (x :@: y) = undefined
toInt :: Finite n -> Int
toInt = getFinite .> fromIntegral

--------------------------------------------------
-- Task 2
--------------------------------------------------

-- 845ms±5ms max 3MiB heap size when serial and not threaded
-- 750ms±100ms max 7MiB heap size with threaded + Async parsing each bingo card
-- On balance, not enough speedup to be worth the extra resources
-- Probably could do better with some chunking
runTask2 :: IO (Maybe Int)
runTask2
  = rawInput
      |> parseDay4
      |> winningBoardScores
      |> filter isJust
      -- |> trace print
      |> last
      |> fmap join