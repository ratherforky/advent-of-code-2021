{-# language NoImplicitPrelude, LambdaCase #-}
module Day2 where

import MyStreamingPrelude -- Based on Streamly rather than lists

import Util
import Parsing


-- >>> 1 + 2

--------------------------------------------------
-- Get Input
--------------------------------------------------

data Command
  = Fw Int
  | Dn Int
  | Up Int
  deriving (Eq, Show)

command :: MonadCatch m => Parser m Char Command
command
   = Fw <$ tok "forward" <*> int
 <|> Dn <$ tok "down"    <*> int
 <|> Up <$ tok "up"      <*> int
 <|> die "Command failed to parse"

input :: Serial Command
input = parseInputLines "src/inputs/day2.txt" (entirely command)

--------------------------------------------------
-- Task 1
--------------------------------------------------

runTask1 :: IO Int
runTask1 = input |> task1

task1 :: Monad m => SerialT m Command -> m Int
task1 commands
  = foldl' (\(x, y) -> \case
                    Fw dx -> (x + dx, y)
                    Dn dy -> (x, y + dy)
                    Up dy -> (x, y - dy)
           ) (0,0) commands
  <&> uncurry (*)

--------------------------------------------------
-- Task 2
--------------------------------------------------

runTask2 :: IO Int
runTask2
  = input
 |> task2

task2 :: Serial Command -> IO Int
task2 commands
  = foldl' (\(x, y, aim) -> \case
                    Fw k  -> (x + k, y + k*aim, aim)
                    Dn dy -> (x, y, aim + dy)
                    Up dy -> (x, y, aim - dy)
           ) (0,0,0) commands
  <&> (\(x, y, aim) -> x * y)

-- Debugging version, kept for posterity
-- task2 :: Serial Command -> IO Int
-- task2 commands
--   = (scanl' (\(x, y, aim) -> \case
--                     Fw k  -> (x + k, y + k*aim, aim)
--                     Dn dy -> (x, y, aim + dy)
--                     Up dy -> (x, y, aim - dy)
--            ) (0,0,0) commands
--   |> trace print
--   |> last)
--   <&> fromMaybe (0,0,0)
--   <&> (\(x, y, aim) -> x * y)