{-# LANGUAGE TypeApplications #-}
{-# language NoImplicitPrelude, ScopedTypeVariables #-}
module Parsing
  ( module Parsing
  , module Streamly.Internal.Unicode.Char.Parser
  , Parser.die
  , Parser.Parser
  , Parser.toFold
  , Parser.fromFold
  , parse
  ) where

import MyStreamingPrelude hiding (many, some)
import Util

import Streamly.Internal.Data.Parser as Parser
import qualified Prelude as Prel
import qualified Streamly.Data.Fold as Fold
import Streamly.Internal.Unicode.Char.Parser hiding (print)
import Streamly.Internal.Data.Stream.IsStream.Eliminate ( parse )
import qualified Streamly.Internal.Unicode.Array.Char as UnicodeA
import qualified Streamly.Internal.Data.Stream.IsStream.Eliminate as StreamElim
import qualified Streamly.Internal.Data.Array.Foreign.Type as Arr

-- Type level stuff

import GHC.TypeNats
import Data.Type.Natural
import Data.Vector.Sized (Vector)
import qualified Data.Vector.Sized as V
import GHC.Natural
import Control.Monad.Catch ( MonadThrow(throwM) )
import Control.Exception ( AssertionFailed(AssertionFailed) )


oneOf :: (MonadCatch m, Eq a) => [a] -> Parser m a a
oneOf posibilities = satisfy (`Prel.elem` posibilities)

-- digit :: MonadCatch m => Parser m Char Int
-- digit = digitToInt <$> oneOf ['0'..'9']

int :: MonadCatch m => Parser m Char Int
int = decimal
-- int = Parser.some digit foldInt
--   where
--     foldInt = Fold.foldl' (\acc x -> acc * 10 + x) 0


-- Ensures the full input is consumed, or else fail
entirely :: MonadCatch m => Parser m a b -> Parser m a b
entirely parser = parser <* eof

newline :: MonadCatch m => Parser m Char Char
newline = satisfy (== '\n')

whitespace :: MonadCatch m => Parser m Char ()
whitespace = many (satisfy (`Prel.elem` [' ', '\t', '\n', '\r']))
                  (constFullFold ())

tok :: MonadCatch m => String -> Parser m Char ()
tok = fromList @Identity -- Serial Char
   .> foldl' (\accParser c -> accParser <* char c) (pure ())
   .> runIdentity
   .> (whitespace *>)
   .> (<* whitespace)
  -- where
    -- foo :: Monad m => Fold m Char (Parser m Char ())
    -- foo = foldl' (\accParser c -> accParser <* char c) (pure ())

parseString :: MonadThrow m => Parser m Char a -> String -> m a
parseString parser str = parse parser (fromList str)

-- This version allows for better error messages on a failed parse
parseInputLines :: FilePath -> Parser IO Char a -> Serial a
parseInputLines path parser
  = readChars path
 |> UnicodeA.lines
 |> mapM (Arr.toStream .> StreamElim.parse parser)

-- Day 4

-- csvLine :: MonadCatch m => Parser m Char Int
-- csvLine = int <* char ','
